# rnaQCtools: RNA sequencing QC tools created by DCI Bioinformatics

## Build and install instructions

1. Clone the repo
2. Make the package tar file with ...
```
cd rnaqctools
make all
```
3. Install the `.tar.gz` file in R as you would for any other R package. For example, from the command line you could run `R CMD INSTALL rnaqctools_{$version}.tar.gz`. Alternatively, you could start an R session and run `install.packages('rnaqctools_{$version}.tar.gz', repos=NULL, type='source')`


## Tutorials and examples

The current `vigenette` is available thorugh the `Wiki` tab on the left and in the repo under `rnaqctools/vignettes/myVignettes.Rmd`

This package is used under its former name, `dcibioinfomatics`, in RNAseq QC scripts such as 

- `QC-template.Rnw`: https://gitlab.oit.duke.edu/dcibioinformatics-internal/pipeline/dcibioinformatics-rnaseq/-/blob/master/QC-template.Rnw
- `QC-PLPexample.Rnw`: https://gitlab.oit.duke.edu/dcibioinformatics-internal/pipeline/dcibioinformatics-rnaseq/-/blob/master/QC-PLPexample.Rnw


## Update log

- Changed package name from `rnaSeqQC` to `rnaQCtools` (version 1.10)
- Changed package name from `dcibioinformatics` to `rnaSeqQC` (version 1.9)
